package cmd

import (
	"github.com/spf13/cobra"
)

var listCmd = &cobra.Command{
	Use:   "list",
	Short: "list package",
	Long: `list package, either installed locally or the one I manage on the ledger`,

	Run: func(cmd *cobra.Command, args []string) {
		print("list cmd...")
		// TODO --local flag to list package locally only
		// TODO --own flag to restrict list of package that I'm the author
		// TODO --pending flag to restrict list of package that i'm maintainer and need to be approved
	},
}