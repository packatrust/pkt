package cmd

import (
	"github.com/spf13/cobra"
)

// rootCmd represents the base command when called without any subcommands
var installCmd = &cobra.Command{
	Use:   "install",
	Short: "Install package",
	Long: `Install package with integrity check against blockchain ledger`,

	Run: func(cmd *cobra.Command, args []string) {
		print("Install cmd...")
	},
}