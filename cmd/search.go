package cmd

import (
	"github.com/spf13/cobra"
)

var searchCmd = &cobra.Command{
	Use:   "search",
	Short: "Search package available in the ledger",
	Long: `Install package with integrity check against blockchain ledger`,

	Run: func(cmd *cobra.Command, args []string) {
		print("search cmd...")
	},
}