package cmd

import (
	"github.com/spf13/cobra"
)

var approveCmd = &cobra.Command{
	Use:   "approve",
	Short: "Approve a package signature",
	Long: `Approve package after it has been sign through the smart contract`,

	Run: func(cmd *cobra.Command, args []string) {
		print("approve cmd...")
	},
}