package cmd

import (
	"github.com/spf13/cobra"
)

var signCmd = &cobra.Command{
	Use:   "sign",
	Short: "Sign a package",
	Long: `Sign package on the PackaTrust smart contract`,

	Run: func(cmd *cobra.Command, args []string) {
		print("signature cmd...")
	},
}