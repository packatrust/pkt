package cmd

import (
	"fmt"
	"github.com/spf13/cobra"
	"os"
)

var rootCmd = &cobra.Command{
	Use:   "pkt",
	Short: "PackaTrust package manager",
	Long: `pkt is package manager from PackaTrust to ensure the interoperability of software 
during the build, release, and installation by checking the authenticity in a blockchain`,

	// App main action
	Run: func(cmd *cobra.Command, args []string) {
		/*rpcEndpoint, _ := cmd.Flags().GetString("rpc")

		if rpcEndpoint == "" {
			log.Fatal("Please provide --rpcEndpoint option")
		}*/
	},
}

func init() {
	rootCmd.AddCommand(listCmd)
	rootCmd.AddCommand(searchCmd)
	rootCmd.AddCommand(installCmd)
	rootCmd.AddCommand(approveCmd)
	rootCmd.AddCommand(signCmd)
}

func Execute() {
	if err := rootCmd.Execute(); err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
}