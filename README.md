# PackaTrust Package Manager

## Quick Usage

```
pkt is package manager from PackaTrust to ensure the interoperability of software
during the build, release, and installation by checking the authenticity in a blockchain

Usage:
  pkt [flags]
  pkt [command]

Available Commands:
  approve     Approve a package signature
  help        Help about any command
  install     Install package
  list        list package
  search      Search package available in the ledger
  sign        Sign a package

Flags:
  -h, --help   help for pkt

Use "pkt [command] --help" for more information about a command.
```

## Dev Environment

In order to ease the development using Golang and the Ethereum Blockchain interaction
we implement utility to run the build inside docker container.

### Utility

`./bin/utils`: this utility expose useful utils to compile in a safe environment, the workdir are mount on the repository root path

- go
- golint
- solc
- abigen
- protoc

> Examples
> # Compile the main.go into pkt binary placed on bin directory (Linux platform)
> ./bin/go build -o ../bin/pkt
> 
> # Compile the main.go into pkt binary placed on bin directory (OSX platform)
> GOOS=darwin ./bin/go build -o ../bin/pkt

### Smart Contract Scaffolding

In order to interact with the Solidity contract in GoLang, we will generate the struct based on solidity file.

```
# generate the ABI from a solidity source file
./bin/utils solc --abi contracts/PackaTrust.sol -o contracts

# convert the ABI to a Go file that we can import
./bin/utils abigen --abi=contracts/PackaTrust.abi --pkg=contract --out=internal/contract.go

# compile contract into EVM bytecode in order to encapsulate it in a transaction to deploy the contract
./bin/utils solc --bin contracts/PackaTrust.sol -o contracts

# compile bin + abi
./bin/utils abigen --bin=contracts/PackaTrust.bin --abi=contracts/PackaTrust.abi --pkg=contract --out=internal/contract.go
```
