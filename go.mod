module gitlab.com/packatrust/pkt

go 1.14

require (
	github.com/ethereum/go-ethereum v1.9.22 // indirect
	github.com/spf13/cobra v1.0.0
	github.com/spf13/viper v1.7.0
	golang.org/x/tools v0.0.0-20201002055958-0d28ed0cbe40 // indirect
	golang.org/x/tools/gopls v0.5.0 // indirect
)
