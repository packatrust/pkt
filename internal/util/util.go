package util

func Intersect(left []string, right []string) []string {
	var result []string
	mapper := make(map[string]bool)
	for _, value := range left {
		mapper[value] = true
	}
	for _, value := range right {
		if mapper[value] == false {
			result = append(result, value)
		}
	}
	return result
}
