package util

import (
	"fmt"
	"testing"
)

func TestIntersect(t *testing.T) {
	result := Intersect([]string{"foo", "bar"}, []string{"foo", "baz"})

	if len(result) != 1 || result[0] != "baz" {
		t.Errorf("expected baz not %s", result)
	}

	fmt.Println(result)
}
