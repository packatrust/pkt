#!/usr/bin/env sh

: ${GETH_HOSTNAME:="localhost"}
: ${GETH_RPCPORT:=8545}
: ${GETH_PROTOCOL:="http"}

cd /app
sed -i -e "s/localhost/${GETH_HOSTNAME}/" app/app.js
sed -i -e "s/localhost/0.0.0.0/" package.json

npm start
