# PackaTrust Ethereum PoA Development

This is the PackaTrust Ethereum PoA Development

# Getting started

## 1. Installing

### 1.1. Ethereum Cluster with netstats monitoring

To run an Ethereum Docker cluster run the following:

```
$ docker-compose up -d
```

By default this will create:

* 1 Ethereum Bootstrapped container
* 1 Ethereum container (which connects to the bootstrapped container on launch)
* 1 Netstats container (with a Web UI to view activity in the cluster)

To access the Netstats Web UI:

```
open http://$(docker-machine ip default):3000
```

### Scaling the number of nodes/containers in the cluster

You can scale the number of Ethereum nodes by running:

```
docker-compose scale eth=3
```

This will scale the number of Ethereum nodes **upwards** (replace 3 with however many nodes
you prefer). These nodes will connect to the P2P network (via the bootstrap node)
by default.

### 1.3. Test accounts ready for use

As part of the bootstrapping process we bootstrap 10 Ethereum accounts for use
pre-filled with 20 Ether for use in transactions by default.

If you want to change the amount of Ether for those accounts
See `files/genesis.json`.

### 1.3.1 Creating new Test accounts

```
docker exec -it bootstrap geth account new --datadir /root/.ethereum//devchain
```

The generated file will be stored in the keystore (./files/keystore) folder.
If you want to provision it on the cluster you will need to add it into the ./files/genesis.json file and add a balance.

## 2. Interact with geth

To get attached to the `geth` JavaScript console on the node you can run the following
```
docker exec -it bootstrap geth attach ipc://root/.ethereum/devchain/geth.ipc
```

See the [Javascript Runtime](https://github.com/ethereum/go-ethereum/wiki/JavaScript-Console) docs for more.

### 2.1 Console Tips

### 2.1.1 Get a list of all accounts:

```
eth.accounts
```

### 2.1.2 Unlock an Account (no password) with:
```
web3.personal.unlockAccount(web3.personal.listAccounts[0], null, 60)
```

The testing account password is: pkt

### 2.1.3 Sending a transactions:

```
eth.sendTransaction({from: "0xad0b9cf458aa98ad77103bb5ccffa1c7832347d6",to: "0x310c8aa3a01c8146a3206b3e5cc45f1070346183", value: "7400000000000"})
```

### 2.1.4 Getting transactions informations:

```
eth.getTransaction("0x3884000aaed17e90582eb4bcda3387d26866b60144a2e1cf3dd4f0944e6a37e3")
```

### 2.1.5 Getting Account balance

```
eth.getBalance("0xad0b9cf458aa98ad77103bb5ccffa1c7832347d6")
```

### 2.1.6 transactions inspection and status:

```
txpool.status
txpool.inspect
```

### 2.1.7 Getting Blocks informations:

```
eth.getBlock('pending').number
eth.blockNumber
```

### 2.1.8 PoA requires Signers:

```
clique.getSigners()
clique.propose(<node_id>, true)
```

On the genesis file, the Signers address must be added to the:

```
"extraData": "0x4175677572000000000000000000000000000000000000000000000000000000007ccffb7916f37f7aeef05e8096ecfbe55afc2f0000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000",
```

### 2.1.9 Peers info:

```
admin.peers
admin.nodeInfo
net.peerCount
```

### 2.1.10 Debugging:

```
debug.verbosity(6)
```

### 2.2 Use an existing DAG

To speed up the process, you can use a [pre-generated DAG](https://github.com/ethereum/wiki/wiki/Ethash-DAG). All you need to do is add something like this
```
ADD dag/full-R23-0000000000000000 /root/.ethash/full-R23-0000000000000000
```
to the `monitored-geth-client` Dockerfile.

## 3. Setting up Metamask

### 3.1. Setting the Network

Goto Networks / Custom Networks.

For the New RPC URL add http://$(docker-machine ip default):8545

### 3.2. Setting the account

Goto Import Account / Select Type Json and upload the keystore account file you want to use.

## 4. explorer

Uncomment the explorer service and set the GETH_HOSTNAME variable to the IP of the docker-machine.

You can then access the explorer at:

http://$(docker-machine ip default):8000/#/
