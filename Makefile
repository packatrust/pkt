GO_VERSION="1.14"
ARCH ?= $(shell go env GOOS)-$(shell go env GOARCH)

platform_temp = $(subst -, ,$(ARCH))
GOOS = $(word 1, $(platform_temp))
GOARCH = $(word 2, $(platform_temp))

## CODE STYLE
fmt:
	GOOS=$(GOOS) GOARCH=$(GOARCH) ./bin/utils go fmt internal/**/*.go

lint:
	GOOS=$(GOOS) GOARCH=$(GOARCH) ./bin/utils golint ...

sanity: fmt lint

## BUILD PKT
clean:
	rm -rf bin/pkt

build:
	GOOS=$(GOOS) GOARCH=$(GOARCH) ./bin/utils go build -o ./bin/pkt

## PKT TESTING
test:
	GOARCH=$(GOARCH) ./bin/utils go test -v ./internal/*

## BUILD DOCKER UTILS ( USED AS A DEV SANDBOX )
docker-build-utils:
	docker build -f Dockerfile --build-arg GO_VERSION=$(GO_VERSION) -t packatrust/pkt:utils .

## SCAFFOLD CONTRACT
scaffold:
	./bin/utils solc --abi contracts/PackaTrust.sol -o contracts
	./bin/utils abigen --abi=contracts/PackaTrust.abi --pkg=contract --out=internal/contract.go
	./bin/utils solc --bin contracts/PackaTrust.sol -o contracts
	./bin/utils abigen --bin=contracts/PackaTrust.bin --abi=contracts/PackaTrust.abi --pkg=contract --out=internal/contract.go
	./bin/utils chmod 777 contracts/* internal/contract.go