## IMAGE USED AS FOR DEV ENVIRONMENT
ARG GO_VERSION=1.14

FROM golang:${GO_VERSION}
WORKDIR /app

RUN apt update && apt install -y zip

RUN go get -u golang.org/x/lint/golint

RUN curl -LSs -o /usr/bin/solc https://github.com/ethereum/solidity/releases/download/v0.7.2/solc-static-linux \
    && chmod +x /usr/bin/solc

RUN mkdir /usr/local/src/protoc \
    && cd /usr/local/src/protoc \
    && curl -LSs -o protoc.zip https://github.com/protocolbuffers/protobuf/releases/download/v3.13.0/protoc-3.13.0-linux-x86_64.zip \
    && unzip protoc.zip \
    && rm protoc.zip \
    && ln -s /usr/local/src/protoc/bin/protoc /usr/bin/protoc

RUN go get -u github.com/ethereum/go-ethereum \
    && cd $GOPATH/src/github.com/ethereum/go-ethereum/ \
    && make \
    && make devtools

LABEL io.packatrust="go-dev"
LABEL version="0.1"
LABEL description="PKT package manager dev tools"
